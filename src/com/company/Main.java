package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String statement = null;
        Scanner s = new Scanner(System.in);
        while(true){
            statement = s.nextLine();
            Eliza.listen(statement);
        }

    }
}
