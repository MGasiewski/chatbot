package com.company;

import java.util.Random;

/**
 * Created by matthew on 9/7/2015.
 */
public class RulesModule {
    private static Random r = new Random();

    public static void youAreRule(String s) {
        switch (r.nextInt(3)) {
            case 0: System.out.println(s.replaceFirst(".*([Yy]ou are)|([Yy]ou're)\\b", "Why am I"));
                break;
            case 1: System.out.println(s.replaceFirst(".*([Yy]ou are)|([Yy]ou're)\\b(.*)", "Does it bother you that I am$3?"));
                break;
            case 2: System.out.println(s.replaceFirst(".*([Yy]ou are)|([Yy]ou're)\\b(.*)", "Being$3 is my business."));
                break;
        }
    }
    public static void weAreRule(String s){
        switch(r.nextInt(3)) {
            case 0: System.out.println(s.replaceFirst(".*[Ww]e are\\b(.*)|[Ww]e're\\b(.*)", "Explain how you are$1$2"));
                break;
            case 1: System.out.println(s.replaceFirst(".*[Ww]e are\\b(.*)|[Ww]e're\\b(.*)", "Tell me about a time you were$1$2"));
                break;
            case 2: System.out.println(s.replaceFirst(".*[Ww]e are\\b(.*)|[Ww]e're\\b(.*)", "Do you really believe that you are$1$2?"));
                break;
        }
    }
    public static void theyAreRule(String s){
        switch(r.nextInt(3)) {
            case 0: System.out.println(s.replaceFirst(".*[Tt]hey are\\b(.*)|[Tt]hey're\\b(.*)", "Maybe you can explain why$1$2."));
                break;
            case 1: System.out.println(s.replaceFirst(".*[Tt]hey are\\b(.*)|[Tt]hey're\\b(.*)", "Does their being$1$2 bother you?"));
                break;
            case 2: System.out.println(s.replaceFirst(".*[Tt]hey are\\b(.*)|[Tt]hey're\\b(.*)", "Maybe they have a reason for being$1$2."));
                break;
        }
    }

    public static void isRule(String s){
        switch(r.nextInt(3)) {
            case 0:
                System.out.println(s.replaceFirst(".*\\bis\\b(.*)", "Everyone is$1 once in a while."));
                break;
            case 1:
                System.out.println(s.replaceFirst(".*\\bis\\b(.*)", "Do you think being$1 is bad?"));
                break;
            case 2:
                System.out.println(s.replaceFirst(".*\\bis\\b(.*)", "Maybe there is a reason for being$1."));
                break;
        }
    }

    public static void ruleFive(String s){
        switch(r.nextInt(3)) {
            case 0: System.out.println(s.replaceFirst(".*I am\\b(.*)", "What kind of person is$1?"));
                break;
            case 1: System.out.println(s.replaceFirst(".*I am\\b(.*)", "How are you$1?"));
                break;
            case 2: System.out.println(s.replaceFirst(".*I am\\b(.*)", "Do you think being$1 is noteworthy?"));
                break;
        }
    }

    public static void ruleSix(String s){
        switch(r.nextInt(3)) {
            case 0: System.out.println(s.replaceFirst(".*\\bI\\b(.*)", "You$1?"));
                break;
            case 1: System.out.println(s.replaceFirst(".*\\bI\\b(.*)", "Would you like to tell me why you$1"));
                break;
            case 2: System.out.println(s.replaceFirst(".*\\bI\\b(.*)", "Maybe it's okay that you$1"));
                break;
        }
    }

    public static void ruleSeven(String s){
        switch(r.nextInt(3)) {
            case 0: System.out.println(s.replaceFirst(".*\\b[Yy]ou\\b(.*)", "How about we talk about you?"));
                break;
            case 1: System.out.println(s.replaceFirst(".*\\b[Yy]ou\\b(.*)", "I don't think we need to talk about me."));
                break;
            case 2: System.out.println(s.replaceFirst(".*\\b[Yy]ou\\b(.*)", "That's my business."));
                break;
        }
    }

    public static void someoneRule(String s){
        switch(r.nextInt(3)) {
            case 0:
                System.out.println(s.replaceFirst(".*\\b[Ss]omeone\\b(.*)", "Can you tell me more about this person?"));
                break;
            case 1:
                System.out.println(s.replaceFirst(".*\\b[Ss]omeone\\b(.*)", "Someone?"));
                break;
            case 2:
                System.out.println(s.replaceFirst(".*\\b[Ss]omeone\\b(.*)", "Who is this person?"));
                break;
        }
    }

    public static void helpRule(String s){
        switch(r.nextInt(3)) {
            case 0: System.out.println(s.replaceFirst(".*\\b[Hh]elp\\b(.*)", "How about we talk about you?"));
                break;
            case 1: System.out.println(s.replaceFirst(".*\\b[Hh]elp\\b(.*)", "I don't think we need to talk about me."));
                break;
            case 2: System.out.println(s.replaceFirst(".*\\b[Hh]elp\\b(.*)", "That's my business."));
                break;
        }
    }

    public static void ruleTen(){
        switch(r.nextInt(4)){
            case 0: System.out.println("Maybe you'd like to talk about something else.");
                break;
            case 1: System.out.println("Go on...");
                break;
            case 2: System.out.println("Well, I'm not sure what you want me to do about that.");
                break;
            case 3: System.out.println("Interesting");
        }
    }
}
