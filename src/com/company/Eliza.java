package com.company;

/**
 * Created by matthew on 9/5/2015.
 */
public class Eliza{

    public static void listen(String s){
        s = removeContractions(s);
        if(s.matches(".*\\b[Yy]ou are\\b.*|[Yy]ou're\\b.*"))
            RulesModule.youAreRule(s); //rule one
        else if(s.matches(".*\\b[Ww]e are\\b.*|[Ww]e're\\b.*"))
            RulesModule.weAreRule(s); //rule two
        else if(s.matches(".*\\b[Tt]hey are\\b.*|[Tt]hey're\\b.*"))
            RulesModule.theyAreRule(s);  //rule three
        else if(s.matches(".*\\bis\\b.*"))
            RulesModule.isRule(s); //rule four
        else if(s.matches(".*I am\\b.*"))
            RulesModule.ruleFive(s); //rule five
        else if(s.matches(".*\\bI\\b.*"))
            RulesModule.ruleSix(s); //rule six
        else if(s.matches(".*\\b[Yy]ou\\b.*"))
            RulesModule.ruleSeven(s); //rule seven
        else if(s.matches(".*\\b[Ss]omeone\\b.*"))
            RulesModule.someoneRule(s); //rule eight
        else if(s.matches(".*\\b[Hh]elp\\b.*"))
            RulesModule.helpRule(s); //rule nine
        else
            RulesModule.ruleTen();
    }

    public static String removeContractions(String s){
        s = s.replaceAll("aren't", "are not");
        s = s.replaceAll("can't", "cannot");
        s = s.replaceAll("couldn't", "could not");
        s = s.replaceAll("didn't", "did not");
        s = s.replaceAll("doesn't", "does not");
        s = s.replaceAll("don't", "do not");
        s = s.replaceAll("hadn't", "had not");
        s = s.replaceAll("hasn't", "has not");
        s = s.replaceAll("haven't", "have not");
        s = s.replaceAll("he'd", "he would");
        s = s.replaceAll("he'll", "he will");
        s = s.replaceAll("he's", "he is");
        s = s.replaceAll("I'd", "I would");
        s = s.replaceAll("I'll", "I will");
        s = s.replaceAll("I'm", "I am");
        s = s.replaceAll("I've", "I have");
        s = s.replaceAll("isn't", "is not");
        return s;
    }


}
